<?php

namespace controller;

use \system\ApiFormats\Format;
use \model\User as UserModel;

class Api
{
	# Формат данных
	protected $format = '';

	# Ссылка куда отправлять данные
	protected $url = '';

	# Токен
	protected $token = '';

	function __construct ($app)
	{
		$this->app = $app;
	}

	public function send($data)
	{
		require_once __DIR__ . '/../config/api.php';
        $this->url = $config_api['url'];
        $this->token = $config_api['token_out'];
        $this->format = $config_api['format'];

		$encoder = (new Format())->createData($this->format);
		$request = $this->curl(['user' => $encoder->encode($data)]);
		$request = $encoder->decode($request);
		(new UserModel())->update($data['id'],
		[
			'external_id' => $request['id']
		]);
	}

	public function save()
	{
		$headers = getallheaders();
		if ($headers['Token'] != 'dsfewDfe3g') {
			exit();
		}
		$post = json_decode($_POST['user'], true);

		$user = (new UserModel())->where(['external_id'=>$post['id']]);

		$id = 0;
		if (empty($user)) {
			$id = (new UserModel())->create([
				'external_id' => $post['id'],
				'login'       => $post['login'],
				'pass'        => $post['pass'],
				'email'       => $post['email'],
				'name'        => $post['name'],
				'lastname'    => $post['lastname'],
			]);
		} else {
			(new UserModel())->update($user[0]->id,
			[
				'login'       => $post['login'],
				'pass'        => $post['pass'],
				'email'       => $post['email'],
				'name'        => $post['name'],
				'lastname'    => $post['lastname'],
			]);
			$id = $user[0]->id;
		}

		echo json_encode(['id'=>$id]);
		exit();
	}

	protected function curl($data)
	{
		$curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Token: '.$this->token));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        $out = curl_exec($curl);
        if ( ! $out) {
            return false;
        }
        curl_close($curl);

        return $out;
	}
}
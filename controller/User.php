<?php

namespace controller;

use model\User as UserModel;
use observers\UserObserver;

class User implements \SplSubject
{
	private $observers;

	protected $user = [];
	
	function __construct ($app)
	{
		$this->app = $app;
		$this->observers = new \SplObjectStorage();
		$this->attach(new UserObserver());
	}

	public function index()
	{
		$userModel = (new UserModel())->list();
		return $this->app->view->load('/user/list', ['listUser' => $userModel], true);
	}

	public function add ()
	{
		$content = '';
		$error = [];

		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$this->user = $_POST;
			$error = $this->validateData($_POST['login'], $_POST['email'], $_POST['pass']);

			if (empty($error)) {
				$userModel = (new UserModel())->create([
					'login' => $_POST['login'],
					'pass' => $_POST['pass'],
					'email' => $_POST['email'],
					'name' => $_POST['name'],
					'lastname' => $_POST['lastname'],
			    ]);

				$this->user['id'] = $userModel;
				
				$this->notify();

				if ($userModel) {
					header('Location: ?c=User');
				}
			}

		}

		if (empty($content)) {
			$content =  $this->app->view->load('/user/add', ['method'=>'add', 'user'=>$this->user, 'error'=>$error], true);
		}
		return $content;
	}

	public function edit ()
	{
		$error = [];

		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$this->user = $_POST;
			$this->user['id'] = $_GET['id'];
			$error = $this->validateData($_POST['login'], $_POST['email'], $_POST['pass'], $_GET['id']);

			if (empty($error)) {
				$userModel = (new UserModel())->update($_GET['id'],
				[
					'login' => $_POST['login'],
					'pass' => $_POST['pass'],
					'email' => $_POST['email'],
					'name' => $_POST['name'],
					'lastname' => $_POST['lastname'],
				]);
			
				$this->notify();

				if ($userModel) {
					header('Location: ?c=User');
				}
			}
		}

		$data =(new UserModel())->selectById($_GET['id']);
		$content = $this->app->view->load('/user/add', ['user' => $data, 'error'=>$error,'method'=>'edit'], true);

		return $content;
	}

	public function getUser()
	{
		return $this->user;
	}

	public function validateData($login='', $email='', $pass='', $id=0)
	{
		$error = [];

		$data =(new UserModel())->validLoginEmail($login, $email, $id);

		if (trim($login) == '') {
			$error['login'] = 'Поле логин обязательно для заполнения';
		}

		if (trim($email) == '') {
			$error['email'] = 'Поле email обязательно для заполнения';
		}

		if (!preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/', $email)) {
			$error['email'] = 'Не коректно введен email';
		}

		if (!empty($data) && empty($error)) {
			if ($data['login'] == $login) {
				$error['login'] = 'Такой логин уже существует!';
			}
			if ($data['email'] == $email) {
				$error['email'] = 'Такой email уже существует!';
			}
		}
		if (!preg_match('~^(?=.*[a-zа-яё])(?=.*[A-ZА-ЯЁ])(?=.*\d)~', $pass)) {
			$error['pass'] = 'Пароль должен содержать одну заглавную, одну маленькую и одну цифру.';
		}

		return $error;
	}

	public function attach(\SplObserver $observer)
	{

		$this->observers->attach($observer);
	}

	public function detach(\SplObserver $observer)
	{
		$key = array_search($observer, $this->observers, true);
        if ($key) {
            unset($this->observers[$key]);
        }
	}

	public function notify()
    {
        foreach ($this->observers as $observer) {
            $observer->update($this);
        }
    }

}
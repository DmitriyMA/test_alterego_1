<div class="container">
	<form action="" method="POST" id="my_form">
		<div class="form-group row has-danger">
			<label for="login" class="col-sm-2 col-form-label">Логин</label>
			<div class="col-sm-10">
				<input type="text" class="form-control form-control-danger" id="login" name="login" placeholder="Логин" value="<?=$user['login']??''?>">
				<?php if (!empty($error['login'])) : ?>
					<div class="form-control-feedback" style="color: red; text-align: left; width: 100%; position: relative;"><?=$error['login']?></div>
				<?php endif;?>
			</div>
		</div>
		<div class="form-group row">
			<label for="pass" class="col-sm-2 col-form-label">Пароль</label>
			<div class="col-sm-10">
				<input type="password" class="form-control" id="pass" name="pass" placeholder="Пароль" value="<?=$user['pass']??''?>">
				<?php if (!empty($error['pass'])) : ?>
					<div class="form-control-feedback" style="color: red; text-align: left; width: 100%; position: relative;"><?=$error['pass']?></div>
				<?php endif;?>
			</div>
		</div>
		<div class="form-group row">
			<label for="email" class="col-sm-2 col-form-label">Email</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="email" name="email" placeholder="Email" value="<?=$user['email']??''?>">
				<?php if (!empty($error['email'])) : ?>
					<div class="form-control-feedback" style="color: red; text-align: left; width: 100%; position: relative;"><?=$error['email']?></div>
				<?php endif;?>
			</div>
		</div>
		<div class="form-group row">
			<label for="name" class="col-sm-2 col-form-label">Имя</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="name" name="name" placeholder="Имя" value="<?=$user['name']??''?>">
			</div>
		</div>
		<div class="form-group row">
			<label for="lastname" class="col-sm-2 col-form-label">Фамилия</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="lastname" name="lastname" placeholder="Фамилия" value="<?=$user['lastname']??''?>">
			</div>
		</div>
		<div class="form-group row">
		    <div class="col-sm-10">
		    		<button type="submit" class="btn btn-primary"><?=($method=='edit'?'Редактировать':'Добавить')?></button>
			</div>
		</div>
	</form>
</div>
<?php

namespace system;

class DataBase
{
    private $_connection;
    private static $_instance;
    
    public static function getInstance()
    {
        if (!self::$_instance) { // If no instance then make one
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __construct()
    {
        try {
            require_once __DIR__ . '/../config/db.php';
            $host = $config_db['host'];
            $db_name = $config_db['db'];
            $login = $config_db['login'];
            $pass = $config_db['pass'];

            $this->_connection = mysqli_connect($host, $login, $pass);

            if (! $this->_connection)
            {
                $this->error = mysqli_error($db);
                return false;
            }
            mysqli_select_db($this->_connection, $db_name);
            if (! $this->_connection)
            {
                $this->error = mysqli_error($this->_connection);
                return false;
            }
            mysqli_query($this->_connection, 'SET NAMES utf8');
            mysqli_query($this->_connection, 'SET CHARACTER SET utf8');

        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
    
    private function __clone()
    {
    }

    public function __destruct() {
        $this->_connection->close();
    }

    // Получение коннекта с mysql
    public function getConnection()
    {
        return $this->_connection;
    }
}
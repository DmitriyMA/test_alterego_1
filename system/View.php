<?php

namespace system;

class View
{
	public function load($file = '', $data = [], $var = false)
	{
		extract($data);
	  	ob_start();
	    require_once __DIR__ . '/../view/'.$file.'.tpl.php';
	    $echo_content = ob_get_contents();
	  	ob_end_clean();

	  	if ($var) { return $echo_content; }
	  	else echo $echo_content;
	}

}
<?php

namespace system\ApiFormats;

class JsonFormat implements FormatInterface
{
	# encode
    public function encode($input): string
    {
       	return json_encode($input);
    }
    
    # decode
    public function decode(string $input): array
    {
    	return json_decode($input, true);
    }
}
<?php

namespace model;

use system\Db;

class User 
{
	private $table = 'users';

	private $db;

	function __construct ()
	{
		$this->db = new Db;
	}

	public function list()
	{
		return $this->db->get($this->table)->result();
	}

	public function create($data)
	{
		$res = false;
		if ($this->db->insert($this->table, $data) !== false) {
			$res = $this->db->lastId();
		}

		return $res;
	}

	public function update($id, $data)
	{
		return $this->db->where('id', '=', $id)->update($this->table, $data);
	}

	public function selectById($id)
	{
		$data = $this->db->where(['id'=>$id])->get($this->table)->rowArray();
		return $data;
	}

	public function validLoginEmail($login, $email, $id)
	{
		$data = $this->db->where('id', '<>', $id)->where(['login'=>$login])->orWhere(['email'=>$email])->where('id', '<>', $id)->get($this->table)->rowArray();
		return $data;
	}

	public function where($wheres)
	{
		$data = $this->db->where($wheres)->get($this->table)->result();
		return $data;
	}
}
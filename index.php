<?php
session_start();

spl_autoload_register(function ($class_name) {
	$class_name = str_replace('\\', '/', $class_name);
    include $class_name . '.php';
});

$app = new \system\App;

$c = 'User\index';
if (isset($_GET['c'])) {
	$c = $_GET['c'];
} elseif (isset($_POST['c'])) {
	$c = $_POST['c'];
}

$num = strripos($c, '\\');
if ($num === false) {
	$controller = '\controller\\'.$c;
	$method = 'index';
} else {
	$controller = '\controller\\'.substr($c, 0, $num);
	$method = substr($c, -(strlen($c) - $num - 1));
}
$user = (new $controller($app))->$method();

$app->view->load('main', ['content'=>$user]);

?>
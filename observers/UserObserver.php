<?php

namespace observers;

use controller\Api;

class UserObserver implements \SplObserver
{
    public function update(\SplSubject $subject)
    {
        $api = new Api($subject->app);
        $api->send($subject->getUser());
    }
}